//Version: v0.01
package controllers

import (
	"git.forms.io/isaving/sv/ssv9000002/dao"
	"git.forms.io/isaving/sv/ssv9000002/models"
	"git.forms.io/isaving/sv/ssv9000002/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000002Controller struct {
	controllers.CommController
}

func (*Ssv9000002Controller) ControllerName() string {
	return "Ssv9000002Controller"
}

// @Desc ssv9000002 controller
// @Description Entry
// @Param ssv9000002 body models.SSV9000002I true "body for user content"
// @Success 200 {object} models.SSV9000002O
// @router /ssv9000002 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv9000002Controller) Ssv9000002() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000002Controller.Ssv9000002 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000002I := &models.SSV9000002I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000002I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000002I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000002 := &services.Ssv9000002Impl{}
	ssv9000002.New(c.CommController)
	ssv9000002.Sv900002I = ssv9000002I
	ssv9000002.O = dao.EngineCache
	ssv9000002O, err := ssv9000002.Ssv9000002(ssv9000002I)

	if err != nil {
		log.Errorf("Ssv9000002Controller.Ssv9000002 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv9000002O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv9000002 Controller
// @Description ssv9000002 controller
// @Param Ssv9000002 body models.SSV9000002I true body for SSV9000002 content
// @Success 200 {object} models.SSV9000002O
// @router /create [post]
/*func (c *Ssv9000002Controller) SWSsv9000002() {
	//Here is to generate API documentation, no need to implement methods
}*/
