package dao

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/sv/ssv9000002/models"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_pdsbasp struct {
	PrductId         string  `xorm:"'prduct_id' pk"`
	PrductNm         int     `xorm:"'prduct_nm' pk"`
	PrductStatus     string  `xorm:"'prduct_status'"`
	ProductName      string  `xorm:"'product_name'"`
	Currency         string  `xorm:"'currency'"`
	TermFlag         string  `xorm:"'term_flag'"`
	PrductBuyTime    int     `xorm:"'prduct_buy_time'"`
	BlncRflctsDrcton string  `xorm:"'blnc_rflcts_drcton'"`
	AmtOpenMin       float64 `xorm:"'amt_open_min'"`
	DepositNature    string  `xorm:"'deposit_nature'"`
	ApprvlMark       string  `xorm:"'apprvl_mark'"`
	WdrwlMthd        string  `xorm:"'wdrwl_mthd'"`
	AppntDtFlg       string  `xorm:"'appnt_dt_flg'"`
	DepcreFlag       string  `xorm:"'depcre_flag'"`
	SttmntFlg        string  `xorm:"'sttmnt_flg'"`
	SttmntType       string  `xorm:"'sttmnt_type'"`
	SttmntPeriod     string  `xorm:"'sttmnt_period'"`
	SttmntFrequency  int     `xorm:"'sttmnt_frequency'"`
	SttmntDate       int     `xorm:"'sttmnt_date'"`
	SlpTrnChange     string  `xorm:"'slp_trn_change'"`
	StpTrnAut        string  `xorm:"'stp_trn_aut'"`
	StpAmtMin        float64 `xorm:"'stp_amt_min'"`
	StpGraceDate     int     `xorm:"'stp_grace_date'"`
	StpNotDate       int     `xorm:"'stp_not_date'"`
	StpIntFlag       string  `xorm:"'stp_int_flag'"`
	StpUnclaimedDay  int     `xorm:"'stp_unclaimed_day'"`
	CancelAutoFlag   string  `xorm:"'cancel_auto_flag'"`
	CancelAutoDay    int     `xorm:"'cancel_auto_day'"`
	CancelRepnFlag   string  `xorm:"'cancel_repn_flag'"`
	TccStatus        int     `xorm:"'tcc_status'"`
}

func (t *T_pdsbasp) TableName() string {
	return "t_pdsbasp"
}

func QueryT_pdsbaspById(SSV9000002I *models.SSV9000002I, o *xorm.Engine) (t_pdsbasp *T_pdsbasp, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_pdsbasp panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()

	t_pdsbasp = &T_pdsbasp{
		PrductId:  SSV9000002I.PrductId,
		PrductNm:  SSV9000002I.PrductNm,
		Currency:  SSV9000002I.CUR,
		TccStatus: 0,
		TermFlag:  SSV9000002I.PeriodFlag,
	}
	log.Debug("查询的条件：", t_pdsbasp)

	exists, err := o.Get(t_pdsbasp)
	if err != nil {
		log.Info("不存在该产品信息")
		return nil, err
	}
	if !exists {
		log.Info("记录为空")
		return nil, nil
	}
	return t_pdsbasp, nil

}
