package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060007I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"`
	PrinStatus  string `valid:"MaxSize(2)"`
	ValidFlag   string `valid:"MaxSize(1)"`
}

type AC060007O struct {
	AcctgAcctNo string
	ArrRecCnt   int                // 数组记录条数
	Records     []AC060007Orecords // 返回的主要数组信息
}

type AC060007Orecords struct {
	IntRate             float64
	Rate                float64
	PrinStatus          string
	AmtLvlsFlag         string      `json:"AmtLvlsFlag"`
	Back1               interface{} `json:"Back1"`
	Back2               interface{} `json:"Back2"`
	Back3               interface{} `json:"Back3"`
	BankNo              string      `json:"BankNo"`
	CornerFlag          string      `json:"CornerFlag"`
	CritocalPointFlag   string      `json:"CritocalPointFlag"`
	Currency            string      `json:"Currency"`
	DefFloatRate        float64     `json:"DefFloatRate"`
	FixdIntRate         float64     `json:"FixdIntRate"`
	Flag1               interface{} `json:"Flag1"`
	Flag2               interface{} `json:"Flag2"`
	FloatCeil           float64     `json:"FloatCeil"`
	FloatFlag           string      `json:"FloatFlag"`
	FloatFloor          float64     `json:"FloatFloor"`
	FloatOption         string      `json:"FloatOption"`
	IntCalcOption       string      `json:"IntCalcOption"`
	IntFlag             string      `json:"IntFlag"`
	IntPlanDesc         interface{} `json:"IntPlanDesc"`
	IntPlanNo           string      `json:"IntPlanNo"`
	IntRateNo           string      `json:"IntRateNo"`
	IntRateUseFlag      string      `json:"IntRateUseFlag"`
	LastMaintBrno       interface{} `json:"LastMaintBrno"`
	LastMaintDate       interface{} `json:"LastMaintDate"`
	LastMaintTell       interface{} `json:"LastMaintTell"`
	LastMaintTime       interface{} `json:"LastMaintTime"`
	LoanIntRateAdjCycle string      `json:"LoanIntRateAdjCycle"`
	LoanIntRateAdjFreq  string      `json:"LoanIntRateAdjFreq"`
	LoanIntRateAdjType  string      `json:"LoanIntRateAdjType"`
	LvlsIntRateAmtType  string      `json:"LvlsIntRateAmtType"`
	LyrdFlag            string      `json:"LyrdFlag"`
	LyrdTermUnit        string      `json:"LyrdTermUnit"`
	MinIntAmt           float64     `json:"MinIntAmt"`
	MinIntRate          float64     `json:"MinIntRate"`
	MonIntUnit          string      `json:"MonIntUnit"`
	TccState            interface{} `json:"TccState"`
	TermAmtPrtyFlag     string      `json:"TermAmtPrtyFlag"`
	TermLvlsFlag        string      `json:"TermLvlsFlag"`
	TolLvls             int         `json:"TolLvls"`
	YrIntUnit           string      `json:"YrIntUnit"`
}

type AC060007IDataForm struct {
	FormHead CommonFormHead
	FormData AC060007I
}

type AC060007ODataForm struct {
	FormHead CommonFormHead
	FormData AC060007O
}

type AC060007RequestForm struct {
	Form []AC060007IDataForm
}

type AC060007ResponseForm struct {
	Form []AC060007ODataForm
}

// @Desc Build request message
func (o *AC060007RequestForm) PackRequest(AC060007I AC060007I) (responseBody []byte, err error) {

	requestForm := AC060007RequestForm{
		Form: []AC060007IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060007I",
				},
				FormData: AC060007I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060007RequestForm) UnPackRequest(request []byte) (AC060007I, error) {
	AC060007I := AC060007I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060007I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060007I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060007ResponseForm) PackResponse(AC060007O AC060007O) (responseBody []byte, err error) {
	responseForm := AC060007ResponseForm{
		Form: []AC060007ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060007O",
				},
				FormData: AC060007O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060007ResponseForm) UnPackResponse(request []byte) (AC060007O, error) {

	AC060007O := AC060007O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060007O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060007O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060007I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
