package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL100080I []struct {
	AcctiAcctNo		string
	Pridnum			string
}

type IL100080O struct {

}

type IL100080IDataForm struct {
	FormHead CommonFormHead
	FormData IL100080I
}

type IL100080ODataForm struct {
	FormHead CommonFormHead
	FormData IL100080O
}

type IL100080RequestForm struct {
	Form []IL100080IDataForm
}

type IL100080ResponseForm struct {
	Form []IL100080ODataForm
}

// @Desc Build request message
func (o *IL100080RequestForm) PackRequest(IL100080I IL100080I) (responseBody []byte, err error) {

	requestForm := IL100080RequestForm{
		Form: []IL100080IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL100080I",
				},
				FormData: IL100080I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL100080RequestForm) UnPackRequest(request []byte) (IL100080I, error) {
	IL100080I := IL100080I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL100080I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL100080I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL100080ResponseForm) PackResponse(IL100080O IL100080O) (responseBody []byte, err error) {
	responseForm := IL100080ResponseForm{
		Form: []IL100080ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL100080O",
				},
				FormData: IL100080O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL100080ResponseForm) UnPackResponse(request []byte) (IL100080O, error) {

	IL100080O := IL100080O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL100080O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL100080O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL100080I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
