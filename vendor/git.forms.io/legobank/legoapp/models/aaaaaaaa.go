package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AAAAAAAAI struct {

}

type AAAAAAAAO struct {

}

type AAAAAAAAIDataForm struct {
	FormHead CommonFormHead
	FormData AAAAAAAAI
}

type AAAAAAAAODataForm struct {
	FormHead CommonFormHead
	FormData AAAAAAAAO
}

type AAAAAAAARequestForm struct {
	Form []AAAAAAAAIDataForm
}

type AAAAAAAAResponseForm struct {
	Form []AAAAAAAAODataForm
}

// @Desc Build request message
func (o *AAAAAAAARequestForm) PackRequest(AAAAAAAAI AAAAAAAAI) (responseBody []byte, err error) {

	requestForm := AAAAAAAARequestForm{
		Form: []AAAAAAAAIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AAAAAAAAI",
				},
				FormData: AAAAAAAAI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AAAAAAAARequestForm) UnPackRequest(request []byte) (AAAAAAAAI, error) {
	AAAAAAAAI := AAAAAAAAI{}
	if err := json.Unmarshal(request, o); nil != err {
		return AAAAAAAAI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AAAAAAAAI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AAAAAAAAResponseForm) PackResponse(AAAAAAAAO AAAAAAAAO) (responseBody []byte, err error) {
	responseForm := AAAAAAAAResponseForm{
		Form: []AAAAAAAAODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AAAAAAAAO",
				},
				FormData: AAAAAAAAO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AAAAAAAAResponseForm) UnPackResponse(request []byte) (AAAAAAAAO, error) {

	AAAAAAAAO := AAAAAAAAO{}

	if err := json.Unmarshal(request, o); nil != err {
		return AAAAAAAAO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AAAAAAAAO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AAAAAAAAI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
