package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRDP0I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
}

type DAACRDP0O struct {
	AccruedIntAmt         int         `json:"AccruedIntAmt"`
	AcctOpenDate          string      `json:"AcctOpenDate"`
	AcctStatus            string      `json:"AcctStatus"`
	AcctType              string      `json:"AcctType"`
	AcctgAcctNo           string      `json:"AcctgAcctNo"`
	BanknoteFlag          string      `json:"BanknoteFlag"`
	ContID                string      `json:"ContId"`
	Currency              string      `json:"Currency"`
	CurrencyMarket        string      `json:"CurrencyMarket"`
	CurrentAcctBal        float64     `json:"CurrentAcctBal"`
	CurrentAcctBalTemp1   interface{} `json:"CurrentAcctBalTemp1"`
	CurrentAcctBalTemp2   interface{} `json:"CurrentAcctBalTemp2"`
	CustID                string      `json:"CustId"`
	CustLastEventDate     string      `json:"CustLastEventDate"`
	CustType              string      `json:"CustType"`
	DayCashInAmt          int         `json:"DayCashInAmt"`
	DayCashOutAmt         int         `json:"DayCashOutAmt"`
	DayCreditAmt          int         `json:"DayCreditAmt"`
	DayCreditCount        int         `json:"DayCreditCount"`
	DayDebitAmt           int         `json:"DayDebitAmt"`
	DayDebitCount         int         `json:"DayDebitCount"`
	DayTranCnt            int         `json:"DayTranCnt"`
	DayTranInAmt          int         `json:"DayTranInAmt"`
	DayTranOutAmt         int         `json:"DayTranOutAmt"`
	DebitFlag             string      `json:"DebitFlag"`
	FirstDepDate          string      `json:"FirstDepDate"`
	FrozenAmt             int         `json:"FrozenAmt"`
	IntPaidAmt            int         `json:"IntPaidAmt"`
	IntrPlanNo            interface{} `json:"IntrPlanNo"`
	IsAllowOverdraft      string      `json:"IsAllowOverdraft"`
	IsCalInt              string      `json:"IsCalInt"`
	LastAccruedDate       string      `json:"LastAccruedDate"`
	LastAcctBal           int         `json:"LastAcctBal"`
	LastEventDate         string      `json:"LastEventDate"`
	LastMaintBrno         string      `json:"LastMaintBrno"`
	LastMaintDate         string      `json:"LastMaintDate"`
	LastMaintTell         string      `json:"LastMaintTell"`
	LastMaintTime         string      `json:"LastMaintTime"`
	LastRestAmt           int         `json:"LastRestAmt"`
	LastRestBeginDate     string      `json:"LastRestBeginDate"`
	LastRestDate          string      `json:"LastRestDate"`
	LastSubmitDate        string      `json:"LastSubmitDate"`
	LastTotDays           string      `json:"LastTotDays"`
	NextAccrualDate       string      `json:"NextAccrualDate"`
	NextRestDate          string      `json:"NextRestDate"`
	OrgID                 string      `json:"OrgId"`
	ProdCode              string      `json:"ProdCode"`
	ReservedAmt           int         `json:"ReservedAmt"`
	RestPeriodAccruedDays int         `json:"RestPeriodAccruedDays"`
	SumCurrentBal         int         `json:"SumCurrentBal"`
	SumPeriodicBal        int         `json:"SumPeriodicBal"`
	TccState              int         `json:"TccState"`
}

type DAACRDP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRDP0I
}

type DAACRDP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRDP0O
}

type DAACRDP0RequestForm struct {
	Form []DAACRDP0IDataForm
}

type DAACRDP0ResponseForm struct {
	Form []DAACRDP0ODataForm
}

// @Desc Build request message
func (o *DAACRDP0RequestForm) PackRequest(DAACRDP0I DAACRDP0I) (responseBody []byte, err error) {

	requestForm := DAACRDP0RequestForm{
		Form: []DAACRDP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRDP0I",
				},
				FormData: DAACRDP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRDP0RequestForm) UnPackRequest(request []byte) (DAACRDP0I, error) {
	DAACRDP0I := DAACRDP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRDP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRDP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRDP0ResponseForm) PackResponse(DAACRDP0O DAACRDP0O) (responseBody []byte, err error) {
	responseForm := DAACRDP0ResponseForm{
		Form: []DAACRDP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRDP0O",
				},
				FormData: DAACRDP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRDP0ResponseForm) UnPackResponse(request []byte) (DAACRDP0O, error) {

	DAACRDP0O := DAACRDP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRDP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRDP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRDP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
