//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type EUL00001I struct {
	SessionId string `json:"sessionId" validate:"required" description:"web session id"`
}

type EUL00001O struct {
	UserLoginId      string `json:"userLoginId" description:"useLoginId which storage in cache when login"`
	PartyId          string `json:"partyId" description:"partyId which storage in cache when login"`
	CorporatePartyId string `json:"corporatePartyId" description:"corporatePartyId which storage in cache when login"`
	RoleType         string `json:"roleType" description:"roleType which storage in cache when login"`
	ProductStoreId   string `json:"productStoreId" description:"storeId which storage in cache when login"`
	PosId            string `json:"posId" description:"posId of device"`
}

// @Desc Build request message
func (o *EUL00001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *EUL00001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *EUL00001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *EUL00001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *EUL00001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*EUL00001I) GetServiceKey() string {
	return "eul00001"
}
