//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type ESC20005I struct {
	ShoppingCartId string  `json:"shoppingCartId" validate:"required" description:"id of shopping cart"`
	ProductStoreId string  `json:"productStoreId" validate:"required" description:"id of product store"`
	ProductId      string  `json:"productId" validate:"required" description:"id of product"`
	Quantity       float64 `json:"quantity" description:"quantity of product"`
	ModifiedPrice  float64 `json:"modifiedPrice" description:"price of product"`
	IsSelected     bool    `json:"isSelected" description:"is select to order"`
	UserLoginId    string  `json:"userLoginId" validate:"required" description:"userLoginId which query from EUL00001"`
}

type ESC20005O struct {
	ShoppingCartId        string `json:"shoppingCartId" description:"id of shopping cart"`
	ShoppingCartItemSeqId string `json:"shoppingCartItemSeqId" description:"seq id of shopping cart item"`
}

// @Desc Build request message
func (o *ESC20005I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *ESC20005I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *ESC20005O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ESC20005O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *ESC20005I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*ESC20005I) GetServiceKey() string {
	return "esc20005"
}
