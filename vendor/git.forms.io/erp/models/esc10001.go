//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type ESC10001I struct {
	CartName            string `json:"cartName" description:"name of shopping cart"`
	CartType            string `json:"cartType" description:"type of shopping cart"`
	UserLoginId         string `json:"userLoginId" validate:"required" description:"userLoginId"`
	LinkedMemberPartyId string `json:"linkedMemberPartyId" description:"partyId of member"`
	CorporatePartyId    string `json:"corporatePartyId" description:"partyId of corporate"`
	ProductStoreId      string `json:"productStoreId" description:"id of product store"`
	TicketId            string `json:"ticketId" description:"id of ticket"`
	CurrencyUom         string `json:"currencyUom" description:"uom"`
	Description         string `json:"description" description:"description"`
	CurrentCartCount    int `json:"currentCartCount" description:"shopping cart count"`
}

type ESC10001O struct {
	CartId string `json:"cartId" description:"id of shopping cart"`
}

// @Desc Build request message
func (o *ESC10001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *ESC10001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *ESC10001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ESC10001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *ESC10001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*ESC10001I) GetServiceKey() string {
	return "esc10001"
}
