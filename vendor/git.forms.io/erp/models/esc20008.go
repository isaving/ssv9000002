//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
	"time"
)

type ESC20008I struct {
	ShoppingCartId string `json:"shoppingCartId"`
	UserLoginId    string `json:"userLoginId"`
}

type ESC20008O struct {
	ShoppingCartId      string    `json:"shoppingCartId"`
	ShoppingCartType    string    `json:"shoppingCartType"`
	ShoppingCartName    string    `json:"shoppingCartName"`
	SessionId           string    `json:"sessionId"`
	ActivityId          string    `json:"activityId"`
	UserLoginId         string    `json:"userLoginId"`
	LinkedMemberPartyId string    `json:"linkedMemberPartyId"`
	CorporatePartyId    string    `json:"corporatePartyId"`
	ProductStoreId      string    `json:"productStoreId"`
	Channel             string    `json:"channel"`
	TicketId            string    `json:"ticketId"`
	Description         string    `json:"description"`
	Status              string    `json:"status"`
	CurrencyUom         string    `json:"currencyUom"`
	FromDate            string    `json:"fromDate"`
	ThruDate            string    `json:"thruDate"`
	CreatedBy           string    `json:"createdBy"`
	CreatedTime         string    `json:"createdTime"`
	UpdatedBy           string    `json:"updatedBy"`
	UpdatedTime         time.Time `json:"updatedTime"`
}

// @Desc Build request message
func (o *ESC20008I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *ESC20008I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *ESC20008O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ESC20008O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *ESC20008I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*ESC20008I) GetServiceKey() string {
	return "esc20008"
}
