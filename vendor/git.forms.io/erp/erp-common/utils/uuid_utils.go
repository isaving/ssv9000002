package utils

import (
	uuid "github.com/satori/go.uuid"
	"strings"
)

func GetUUID() string {
	u1 := uuid.NewV4()
	str := strings.ReplaceAll(u1.String(), "-", "")
	return strings.ToUpper(str)
}
