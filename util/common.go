package util

import (
	"errors"
)

// get string value from interface{}.
func GetString(v interface{}) (str string, err error) {
	switch v.(type) {
	case string:
		return v.(string), nil
	default:
		return "", errors.New("Invalid string type!")
	}
}

// get int value from interface{}.
func GetInt(v interface{}) (i int, err error) {
	switch v.(type) {
	case int:
		return v.(int), nil
	default:
		return 0, errors.New("Invalid int type!")
	}
}

func GetInt64(v interface{}) (i int64, err error) {
	switch v.(type) {
	case int64:
		return v.(int64), nil
	default:
		return 0, errors.New("Invalid int64 type!")
	}
}

// get float64 value from interface{}.
func GetFloat64(v interface{}) (f float64, err error) {
	switch v.(type) {
	case float64:
		return v.(float64), nil
	default:
		return 0.0, errors.New("Invalid float64 type!")
	}
}

// get error value from interface{}.
func GetError(v interface{}) (errResult error, err error) {
	switch v.(type) {
	case error:
		return v.(error), nil
	default:
		return nil, errors.New("Invalid error type!")
	}
}

// get map[string]interface{} value from interface{}.
func GetMap(v interface{}) (m map[string]interface{}, err error) {
	switch v.(type) {
	case map[string]interface{}:
		return v.(map[string]interface{}), nil
	default:
		return nil, errors.New("Invalid map type!")
	}
}
