//Version: v0.01
package services

import (
	"git.forms.io/isaving/sv/ssv9000002/constant"
	"git.forms.io/isaving/sv/ssv9000002/dao"
	"git.forms.io/isaving/sv/ssv9000002/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type Ssv9000002Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Sv900002O      *models.SSV9000002O
	Sv900002I      *models.SSV9000002I
	CommonResponse *models.CommonResponse
	O              *xorm.Engine
}

// @Desc Ssv9000002 process
// @Author
// @Date 2020-12-04
func (impl *Ssv9000002Impl) Ssv9000002(ssv9000002I *models.SSV9000002I) (ssv9000002O *models.SSV9000002O, err error) {

	impl.Sv900002I = ssv9000002I
	//TODO Service Business Process
	//查询产品基础信息表 t_pdsbasp 不存在报“不存在该产品信息” 有记录，且term_flag	产品分期标志为2-分期则查询产品分期表
	QueryT_pdsbaspById, err := dao.QueryT_pdsbaspById(impl.Sv900002I, impl.O)
	if err != nil {
		log.Info("server Find T_pdsbasp 错误!")
		return nil, err
	}
	log.Debug("查询返回的数据是", QueryT_pdsbaspById)
	if QueryT_pdsbaspById == nil {
		log.Info("查询返回的数据是", QueryT_pdsbaspById)
		return nil, errors.New("Records not found",constant.ERRCODE1)
	}

	if QueryT_pdsbaspById.TermFlag == "2" {
		//查询产品分期信息表   不存在报“产品分期信息表有误”
	}

	ssv9000002O = &models.SSV9000002O{
		//TODO Assign  value to the OUTPUT Struct field
		PrductStatus:            QueryT_pdsbaspById.PrductStatus,
		ProductNameCh:           QueryT_pdsbaspById.ProductName,
		ProductNameEn:           QueryT_pdsbaspById.ProductName,
		Currency:                QueryT_pdsbaspById.Currency,
		OffShoreOnshoreSign:     "",
		TermFlag:                QueryT_pdsbaspById.TermFlag,
		PrductBuyTime:           QueryT_pdsbaspById.PrductBuyTime,
		BlncRflctsDrcton:        QueryT_pdsbaspById.BlncRflctsDrcton,
		AmtOpenMin:              QueryT_pdsbaspById.AmtOpenMin,
		DepositNature:           QueryT_pdsbaspById.DepositNature,
		ApprvlMark:              QueryT_pdsbaspById.ApprvlMark,
		WdrwlMthd:               QueryT_pdsbaspById.WdrwlMthd,
		TrnWdrwmThd:             "",
		AppntDtFlg:              QueryT_pdsbaspById.AppntDtFlg,
		DepcreFlag:              QueryT_pdsbaspById.DepcreFlag,
		SttmntFlg:               QueryT_pdsbaspById.SttmntFlg,
		SttmntType:              QueryT_pdsbaspById.SttmntType,
		SttmntPeriod:            QueryT_pdsbaspById.SttmntPeriod,
		SttmntFrequency:         QueryT_pdsbaspById.SttmntFrequency,
		SttmntDate:              QueryT_pdsbaspById.SttmntDate,
		SlpTrnChange:            QueryT_pdsbaspById.SlpTrnChange,
		StpTrnAut:               QueryT_pdsbaspById.StpTrnAut,
		StpAmtMin:               QueryT_pdsbaspById.StpAmtMin,
		StpGraceDate:            QueryT_pdsbaspById.StpGraceDate,
		StpNotDate:              QueryT_pdsbaspById.StpNotDate,
		StpIntFlag:              QueryT_pdsbaspById.StpIntFlag,
		StpUnclaimedDay:         QueryT_pdsbaspById.StpUnclaimedDay,
		CancelAutoFlag:          QueryT_pdsbaspById.CancelAutoFlag,
		CancelAutoDay:           QueryT_pdsbaspById.CancelAutoDay,
		CancelRepnFlag:          QueryT_pdsbaspById.CancelRepnFlag,
		CashAccessFlag:          "",
		AmtDepositMin:           0,
		ProductNum:              0,
		DepositDayMin:           0,
		DepositDayMax:           0,
		DepositPeriodUnit:       "",
		DepositPeriod:           0,
		DepositAgainFlag:        "",
		AdvanceWithdrawNum:      0,
		MaturityWithdrawNum:     0,
		OverdueWithdrawNum:      0,
		AdvanceWithdrawGraceNum: 0,
		WithdrawAmtMin:          0,
		KeepAmtMin:              0,
		MaturityFlag:            "",
		ConDepositNum:           0,
		AmtConMin:               0,
		BalprocessWay:           "",
		BalConMatWay:            "",
		PrinConMatWay:           "",
		HolidayFlag:             "",
	}
	return ssv9000002O, nil
}
